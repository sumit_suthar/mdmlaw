<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mdmlaw');

/** MySQL database username */
define('DB_USER', 'mdmlaw');

/** MySQL database password */
define('DB_PASSWORD', 'Ek!p3E$z7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$e{Ls8~x5f>P~vP CHRkAblf?Dl-Ws+lR)ppvc:YLdM/$C</FSXx!SUqBd/}<)C9');
define('SECURE_AUTH_KEY',  'C=DDwIK@H<(,KN%|}C&%#WiJ)MYMsU]X<biDinmO5q8#(,Y%D YPn-%)nz-xLwFS');
define('LOGGED_IN_KEY',    'ETcfRv}8K0:P7$=ghU}b gr3aHF0?8^dUU6QDTQjU3 ?*0p-5|_0lN$-Mogh9#/}');
define('NONCE_KEY',        'a E+ey%/rj/#N8p+zb*Op_;EKfEyO/ n{an6r^`_vAH)pa#X3-#:`}A2)Z5Q13)m');
define('AUTH_SALT',        '+LeaQQx#n)$fR+ngN@*lOk=_`5gu`;kCZ:.>NQ@h~[%O%(s78E&406.sm7~/^*/v');
define('SECURE_AUTH_SALT', 'GW0Y9Q+V0C0(y|(bk|^V-NX4q-J,w[{-zAVDK;pCF$)Q;&E IO<aC/I+I-BI+6c;');
define('LOGGED_IN_SALT',   'b HfMLg+t$.bY02IE7NU|V7FDv`G:}FgE/x_:Z.8Abarxi?lw*vZiF,k/ |wL#T2');
define('NONCE_SALT',       'Q|D$OOM3V.qwH(oo/hbRV@G3yL45ho#OK`3chw35Q_6-/ka@Y3RAb+q?LrLM[*f#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
