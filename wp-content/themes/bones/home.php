<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if ( is_front_page( )) : ?>
								
								<?php if (get_field('images')): ?>
									<div id='slider'>
										<?php $count = 0; 
										while(the_repeater_field('images')): 
											$id = 'slide-'.$count++; ?>
											
											<img src='<?php echo get_sub_field('image') ?>' title='#<?php echo $id ?>' />
											
										<?php endwhile; ?>
									</div>
									
									<?php $count = 0;
									while(the_repeater_field('images')): 
											$id = 'slide-'.$count++; ?>
										
										<div id='<?php echo $id ?>' class='nivo-html-caption'>
											
											<h3 class='nivo-subheader'><?php echo get_sub_field('subheader') ?></h3>
											<h2 class='nivo-header'><?php echo get_sub_field('header') ?></h2>
											<p class='nivo-description'>
												<?php echo get_sub_field('body') ?>
											</p>
											
											<span class='scroll-down'>
												<span class='big'>Scroll Down</span>
												to see our services
											</span>
											
										</div>
										
									<?php endwhile; ?>							
									
								<?php endif; ?>
								
								<div class='the-footer'>
									
									<div class='footer-top-bar full-width'>&nbsp;</div>
									<h3 class='clear foot-title full-width'>
										Legal Services
									</h3>
	
									<div class='footer full-width'>
										<div class='full-width'>
	
										<?php foreach( get_all_category_ids() as $key => $catId ):
											$name = get_the_category_by_ID($catId); 
											$slug = strtolower(substr($name, 0, 3));
											if (strtolower($name) != 'uncategorized'): ?>
														
											<div class='section one-fourth'>
												<hr class='top' />
												<h4><?php echo $name ?></h4>
												<span class='bottom'>&nbsp;</span>
												<a href='/practice-areas-2/#<?php echo $catId ?>' class='readmore'>Read More</a>
											</div>
											
											<?php endif;
										endforeach; ?>
										
											<div class="clear">&nbsp;</div>
	
										</div>
									</div>
									
								</div>
	
							<?php else: ?>					

								<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
								<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
	
									<header class="article-header">
	
										<h1 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
										<p class="byline vcard"><?php
											printf(__('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span> <span class="amp">&</span> filed under %4$s.', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), bones_get_the_author_posts_link(), get_the_category_list(', '));
										?></p>
	
									</header> <!-- end article header -->
	
									<section class="entry-content clearfix">
										<?php the_content(); ?>
									</section> <!-- end article section -->
	
									<footer class="article-footer">
										<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'bonestheme') . '</span> ', ', ', ''); ?></p>
	
									</footer> <!-- end article footer -->
	
									<?php // comments_template(); // uncomment if you want to use them ?>
	
								</article> <!-- end article -->
	
								<?php endwhile; ?>
	
									<?php if (function_exists('bones_page_navi')) { ?>
											<?php bones_page_navi(); ?>
									<?php } else { ?>
											<nav class="wp-prev-next">
													<ul class="clearfix">
														<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
														<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
													</ul>
											</nav>
									<?php } ?>
	
								<?php else : ?>
	
										<article id="post-not-found" class="hentry clearfix">
												<header class="article-header">
													<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
											</header>
												<section class="entry-content">
													<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
											</section>
											<footer class="article-footer">
													<p><?php _e("This is the error message in the index.php template.", "bonestheme"); ?></p>
											</footer>
										</article>
	
								<?php endif; ?>
							
							<?php endif; ?>

						</div> <!-- end #main -->


				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
