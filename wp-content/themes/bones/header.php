<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<link rel="stylesheet" type="text/css" href="/wp-content/themes/bones/library/css/fonts/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="/wp-content/themes/bones/library/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- drop Google Analytics Here -->
		<!-- end analytics -->

		<style type='text/css'>
		
			.f-left { float: left; } .f-right { float: right; }
			.image-container { position: relative; height: 374px; width: 436px; }
			.topLeftCorner, .topRightCorner, .botLeftCorner, .botRightCorner {
				width: 50px; height: 50px;
				position: absolute; 
				z-index: 10; 
				background-repeat: no-repeat; 
				background-position: 100% 100%; }
			.topLeftCorner 	{ background-image: url('/wp-content/themes/bones/library/images/top-left-corner.png'); left: -2px; top: -2px; }
			.topRightCorner { background-image: url('/wp-content/themes/bones/library/images/top-right-corner.png'); right: -2px; top: -2px; }
			.botLeftCorner 	{ background-image: url('/wp-content/themes/bones/library/images/bottom-left-corner.png'); left: -2px; bottom: 3px; }
			.botRightCorner { background-image: url('/wp-content/themes/bones/library/images/bottom-right-corner.png'); right: -2px; bottom: 3px; }
			
			/* tab styling */
			.tab-content h3 { color: #8f201a; }
			.tab-content img { height: 368px; width: 435px; margin: 0; }
			.tab-content p { font-size: 15px; }
			.tab-content ul { 
				font-size: 13px;
				font-weight: bolder;
				list-style-image: url('/wp-content/themes/bones/library/images/list-border.png'); 
				padding-left: 20px;	}
			.tab-content p { 
				padding: 0 0 10px;
				background-image: url('/wp-content/themes/bones/library/images/page-title-border.png');
				background-position: bottom center;
				background-repeat: no-repeat;
				background-size: 100%; }
			.tabs-readmore { 
				background-image: url('/wp-content/themes/bones/library/images/readmore.png'); 
				width: 290px; height: 60px; 
				margin: auto;
				position: relative;
				background-repeat: no-repeat;
				text-transform: uppercase; }
			.tabs-readmore a {
				position: absolute;
				left: 91px; top: 5px;
			}
			.one-half { width: 50%; }		
			.the-footer {
				position: fixed;
				width: 100%;
				z-index: 9999;
				bottom: -350px;
				opacity: .4;
			}
			.sub-menu li { background-color: #ab261a; }
			#container .sub-menu a { color: #fff; }
			#container .sub-menu { border: 0; }	
			#breadcrumbs { float: right; margin-top: 47px; }
			#breadcrumbs li { float: left; margin-right: 10px; }
			footer.site-footer { display: none; }
		</style>

		<link rel='stylesheet' type='text/css' href='/wp-content/themes/bones/library/css/nivo-slider.css' />
		<script type='text/javascript' src='/wp-content/themes/bones/library/js/libs/autocolumn.js'></script>
		<script type='text/javascript' src='/wp-content/themes/bones/library/js/libs/jquery.nivo.slider.pack.js'></script>

		<script type='text/javascript'>
		
			var linkWidth = 140;
			jQuery(document).ready(function($) {
				
				$('#slider').nivoSlider( );

				$('.nav li.top').hover(function() {
		
					$('.nav .active').addClass('tmpActive');
					$('.nav .active').removeClass('active');
					
					var position = $(this).offset( );
					var rightX = $(window).width() - (position.left + linkWidth);
					
					$('#inner-header-bg-1').css('width', position.left + "px");
					$('#inner-header-bg-2').css('width', rightX + "px");
					$('#inner-header-bg-3').css('height', "111px");
					
					$('#inner-header-bg-3')
					    .clearQueue()
					    .stop()
						.animate({
							height: "0"
						}, 550 );					
					
				}, function() {

					$('.tmpActive').addClass('active');
					if ( $('.nav .active a').width() > 1 ) {

						var activePosition = $('.nav .active a').offset( );
						var activeRightX = $(window).width() - (activePosition.left + 140);

						$('#inner-header-bg-1').css('width', activePosition.left + 'px'); // widthOne - 70 + 'px');
						$('#inner-header-bg-2').css('width', activeRightX + 'px'); // widthTwo - 70 + 'px');			

						$('#inner-header-bg-3')
						    .clearQueue()
						    .stop()
							.animate({
								height: "0"
							}, 550 );		
							
					} else {

						$('#inner-header-bg-1').css('width', '50%'); // widthOne - 70 + 'px');
						$('#inner-header-bg-2').css('width', '50%'); // widthTwo - 70 + 'px');
						$('#inner-header-bg-3').css('height', "111px");
					}
					
				});
				
				// 1118
				var animated = false;
				function checkAnimation() {
					if ( jQuery(window).height() + jQuery(window).scrollTop() >= 1080 && !animated) {
						
						animated = true;
						jQuery('.the-footer')
						
						    .clearQueue()
						    .stop()
							.animate({
								opacity: 1,
								bottom: "1px"
							}, 350, function() {
								animated = false;
							} );
							
						animated = false;
							
					} else {
						
						animated = true;
						jQuery('.the-footer')
						
						    .clearQueue()
						    .stop()
							.animate({
								opacity: .4,
								bottom: "-350px"
							}, 350, function() {
								animated = false;
							});
							
						animated = false;
						
					}
				}
				
				checkAnimation(); // run this the first time they hit the page in case there screen is big like Andrews	
				jQuery(window).scroll(function() {
					
					checkAnimation();
				});

			}); 
			
			// cool scroll

		</script>
		
	</head>

	<body <?php body_class(); ?>>

		<div id="container">

			<header class="header" role="banner">

				<div class="header-bg">&nbsp;</div>
				
				<div id="inner-header-bg-1">&nbsp;</div>
				<div id="inner-header-bg-2">&nbsp;</div>
				<div id="inner-header-bg-3">&nbsp;</div>
				
				<div id="inner-header" class="wrap clearfix full-width">

					<!-- to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> -->
					<p id="logo" class="h1"><a href="<?php echo home_url(); ?>" rel="nofollow">
						<img src='<?php echo get_template_directory_uri() ?>/library/images/mdm-logo.png' />
					</a></p>

					<!-- if you'd like to use the site description you can un-comment it below -->
					<?php // bloginfo('description'); ?>

					<form role="search" method="get" id="searchform" action="http://mdm.loc/" _lpchecked="1">
						<input type="text" value="" name="s" id="s" placeholder="Search" />
						<input type="submit" id="searchsubmit" value="" />
					</form>

					<nav role="navigation">
						<?php bones_main_nav(); ?>
					</nav>

				</div> <!-- end #inner-header -->

			</header> <!-- end header -->
			<div class="page-wrap">
