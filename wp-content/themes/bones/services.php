<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>
			
			<script type='text/javascript'>
				jQuery(document).ready(function($) {
					
					$('.tab').click(function(){
						var id = $(this).attr('data-category');
					
						$('.tab').removeClass('active');	
						$(this).addClass('active');
						
						$('.tab-content .content').addClass('hide');
						$('.tab-' + id).removeClass('hide');
					});
				});
			</script>
			
			<div id="content">

				<div id="inner-content" class="wrap clearfix full-width">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="page-title"><?php the_title(); ?></h1>
									<!--<p class="byline vcard"><?php
										printf(__('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(__('F jS, Y', 'bonestheme')), bones_get_the_author_posts_link());
									?></p>-->

									<?php the_post_thumbnail('full') ?>
			
								</header> <!-- end article header -->

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php $content = get_the_content(); 
									$middle = strlen($content) / 2;
									while ( $content[$middle] != ' ' ) 
										$middle++;
									
									$content[$middle] = '^';
									$content = explode('^', $content); 	
									foreach ( $content as $block ): ?>
									
									 <div class="split">
										<?php echo $block ?>
									 </div>
										
									<?php endforeach; ?>
									<div class='clear'>&nbsp;</div>
									<center>
										<img src="/wp-content/uploads/2013/10/sketch.png">
									</center>
									

									
									<div class='tabs'> <!-- start tab section -->
										
										<div class='tab-nav'>
											<ul>		
												
										<?php foreach( get_all_category_ids() as $key => $catId ):
											$name = get_the_category_by_ID($catId); 
											if (strtolower($name) != 'uncategorized'): ?>
	
											<li class='tab <?php if ($key == 0): ?>active<?php endif; ?>' data-category='<?php echo $catId ?>'>
												<a class='tab'>
													<?php echo $name ?>
												</a>
											</li>
																						
											<?php endif;		
										endforeach; ?>
										
											</ul>
										</div>
										
										<div class='tab-content'>
											
										<?php foreach( get_all_category_ids() as $key => $catId ):
											$name = get_the_category_by_ID($catId); 
											if (strtolower($name) != 'uncategorized'): ?>
											
											<div class='tab-<?php echo $catId ?> content <?php if ($key != 0): ?>hide<?php endif; ?>'>
												<div class='one-half f-right'>

													<div class="image-container">

														<?php echo cat_image( $catId, false, 'large' ) ?>

													</div>

												</div>
												<div class='one-half'>
	
													<?php echo category_description( $catId ) ?>
		
													<div class='tabs-readmore a-center'>
														<a href='/'>Read more</a>
													</div>

												</div>
											</div>
											
											<?php endif;
										endforeach; ?>
										
										</div>
										
									</div> <!-- end tab section -->
								
								</section> <!-- end article section -->

								<footer class="article-footer">
									<p class="clearfix"><?php the_tags('<span class="tags">' . __('Tags:', 'bonestheme') . '</span> ', ', ', ''); ?></p>

								</footer> <!-- end article footer -->

								<?php // comments_template(); ?>

							</article> <!-- end article -->

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
											<header class="article-header">
												<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e("This is the error message in the page-custom.php template.", "bonestheme"); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div> <!-- end #main -->

						<?php // get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

		<script type='text/javascript'>
			jQuery(document).ready(function($) {
					
				var hash = window.location.hash;
				hash = hash.replace('#','');
				if ( hash && parseInt(hash) > 0 ) {
					
					$('.tab-content .content').addClass('hide');
					$('li.tab').removeClass('active');
					$('.tab[data-category=' + hash + ']').addClass('active');
					$('.tab-' + hash).removeClass('hide');

 	 				$("html, body").animate({ scrollTop: $(document).height() }, 700);
				}				
			});
		</script>
<?php get_footer(); ?>
