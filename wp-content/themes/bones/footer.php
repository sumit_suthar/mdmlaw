			</div>
			<footer class="footer full-width site-footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">

					<nav role="navigation">
							<?php bones_footer_links(); ?>
									</nav>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>

				</div> <!-- end #inner-footer -->

			</footer> <!-- end footer -->

		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

		<script type='text/javascript'>
			var activePosition = '';
			var activeRightX = '';
			jQuery(document).ready(function($) {
								
				// active state
				activePosition = $('.nav .active a').offset( );
				if ( $('.nav .active a').width() > 1 ) {
					activeRightX = $(window).width() - (activePosition.left + linkWidth);
					$('#inner-header-bg-1').css('width', activePosition.left + "px");
					$('#inner-header-bg-2').css('width', activeRightX + "px");
					$('#inner-header-bg-3').css('height', "0px");
				}
				
				$('.image-container').append('<div class="topLeftCorner ">&nbsp;</div>');
				$('.image-container').append('<div class="topRightCorner">&nbsp;</div>');
				$('.image-container').append('<div class="botLeftCorner ">&nbsp;</div>');
				$('.image-container').append('<div class="botRightCorner">&nbsp;</div>');			
			});
		</script>

	</body>
</html> <!-- end page. what a ride! -->
