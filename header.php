<?php global $options, $theme_data?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title>
<?php if (is_front_page() ) {
bloginfo('name');
} elseif ( is_category() ) {
	single_cat_title(); echo ' - ' ; bloginfo('name');
} elseif (is_single() ) {
	single_post_title();
} elseif (is_page() ) {
	single_post_title(); echo ' - '; bloginfo('name');
} else {
	wp_title('',true);
} ?>
</title>
<?php


?>
<?php
$key = get_post_meta($post->ID,"ws_page_keywod",true);
$desc = get_post_meta($post->ID,"ws_page_desc",true);
$key_main = $options['key_main'];
$desc_main =  $options['desc_main'];

?>


<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta name="keywords" content="<?php echo ($key) ? stripslashes($key) : $key_main ;?>" />
<meta name="description" content="<?php echo ($desc) ? stripslashes($desc) : $desc_main ;?>" />

<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="http://50.87.145.148/~kurtz/wp-content/themes/freska_v1_4/css/custom.css" type="text/css" media="screen" />
<?php $favico = get_option('ws_favicon');?>
<link rel="shortcut icon" href="<?php echo ($favico) ? $favico : get_template_directory_uri().'/images/favicon.ico';?>"/>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if($options['custom_style_enable']=='on') {?>
<?php 	get_template_part( '/css/custom_style'); ?>
<?php }  ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(''); ?>>
<div id="wrapper" <?php if($options['fluid_enable']=='on'){?> class="fluid" <?php }?>>
<div id="header" >
<?php if($options['top_panel_enable']=='on') {?>
<div class="top_header ">

<div class="container">
<?php if($options['header_toll_free']=='on') {?>
<div class="left_info"> <span class="tool_free"> <?php echo $options['toll_free_title'];?> <?php echo $options['toll_free'];?> </span>  <span class="mail_link"><a href="mailto:<?php echo $options['cnt_email'];?>"><?php echo $options['cnt_email'];?></a></span> </div>
<!-- Tool Free !-->

<?php } ?>
<?php if($options['header_social_links']=='on') {?>
<?php 	get_template_part( '/lib/int/socail'); ?>
<?php } ?>
</div>
</div>
<!-- header top !--->
<?php } ?>
<div class="header container clearfix">


<div class="logo_wrapper">
<?php if($options['custom_logo']!=''){?>
<a href="<?php echo home_url();?>"><img src="<?php echo $options['custom_logo'];?>" alt="<?php bloginfo('name');?>" /></a>
<?php } else {?>
<a href="<?php echo home_url();?>"><img src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="<?php bloginfo('name');?>" /></a>
<?php } ?>
<?php $logo_subtitle  = $options['logo_subtitle']; ?>
<span class="logo_tagline"><?php echo $logo_subtitle ; ?></span> </div>
<!-- logo Wrapper !--->

<div class="top_right">
<div id="icon-widgets" class="header_links <?php echo $options['header_icons'];?>">
<ul class="icon-menu ">

	    <?php
		
		$args = array('theme_location'=>'top_right', 'fallback_cb' => '');
			if(wsblogz_woocommerce_enabled()) $args['fallback_cb'] ='wsblogz_shop_nav';
			wp_nav_menu($args); 

?>

  
  <li class="search-box">
    <p> <a href="#" class="search_link"><span></span></a> </p>
    <div id="search-box">
      <?php 	get_template_part( '/lib/int/searchbox'); ?>
    </div>
  </li>
</ul>
<!-- Icon Menu End --> 

</div>
<!-- Top Right !--> 
</div>

</div>
<!-- header top !---> 

<div class="banner">
<div class="container">
<?php 	get_template_part( '/lib/int/nav'); ?>
